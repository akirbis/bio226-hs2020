# Session C2: Overall Goal

In this afternoon you will learn;

1. To read, score and interpret microstaellite data using Genemarker (https://softgenetics.com/downloads.php).
2. To read, score and interpret AFLP data using Genemarker (https://softgenetics.com/downloads.php).
3. To read, score and interpret microsatellite data using the R package Fragman (https://cran.r-project.org/web/packages/Fragman/index.html).
4. To read Sanger trace files, correct them and assemble them into a contig using Sequencher (http://www.genecodes.com/free-download).
5. To read fasta files, align them and intepret their differences using Bioedit (for windows users https://bioedit.software.informer.com/) and Aliview (for mac users https://macdownload.informer.com/aliview/). 

<br />
<br />

   
   

# 1. Analyze microsat data using Genemarker

## Installation

The Genemarker package is a commercial package. It is only available as 15 days demo version. Therefore, please download and install the Genemarker package to your computer first (link to the page see above). Genemarker is only available for windows not for mac. Currently, there is no comparable software available for Mac users. Commerically available softwares provide user-friendly interface and easy visualization solutions which will allow you to quickly and efficiently analyze your data.

## Scoring

You will have to score the microsat data set provided in /home/ubuntu/storage/Comp_sessions/C2/Data_Microsat_fsa/Multiplex_C and /home/ubuntu/storage/Comp_sessions/C2/Data_Microsat_fsa/Multiplex_D. The detailed description of the data set is given in the pdf tutorial.
Please, follow this simple introductory video to read in and score your data (https://www.youtube.com/watch?v=RwkUM71sb4A).

Later on, we will also use the freely available Fragman package to carry out the very same analyses. You will see, that the commercial sofware has several advantages.

<br />
<br />


# 2. Analyze AFLP data using Genemarker

Here, you will have to score the AFLP data provided in the /home/ubuntu/storage/Comp_sessions/C2/Data_AFLP_fsa directory using Genemarker.
You need to follow the very same steps as with the microsat data set. You can also check out the description in the tutorial which was written up for the so-called genographer software. Genographer is not available anymore but the logic of AFLP scoring can be easily transferred to the software Genemarker.


<br />
<br />

# 3. Scoring microsatellite data using the Fragman R package

## Installation

Install it using the install.packages function in R (the name of the package is Fragman) on your own computer.


## Usage

##### Load the library
``
library(Fragman)
``
##### Read in the .fsa files. You have to provide the path to the directory containing the .fsa files. (/home/ubuntu/storage/Comp_sessions/C2/Data_Microsat_fsa/Multiplex_C or Multiplex_D)
``
my.plants <- storing.inds(folder="PATH to your directory")
``
##### Read in the ladder. In this step, you will tell Fragman which ladder peak corresponds to which fragment size. Because for this experiment we used the GS-500-TAMRA ladder (https://www.thermofisher.com/order/catalog/product/401733), the following vector should be entered to calibrate the ladder. Please, verify, whether it is correct.
``
my.ladder <- c(35, 50, 75, 100, 139, 150, 160, 200, 250, 300, 340, 350, 400, 450, 490, 500)
``

##### Attach ladder info to your data set. This function will generate some plots. Try to figure it out whether Fragman properly recognized the peaks of the ladder.
``
ladder.info.attach(stored=my.plants, ladder=my.ladder)
``

##### Do we need to correct the automatic sizing of the ladder peaks? Use the ladder.corrector function if needed. Name of the samples to be corrected should be entered as to.correct="MN214c20_D06_QX.fsa". After that, select manual assignment and follow the instructions on the screen. If more than one correction is needed, you need to reiterate this process until you have fixed all the errors.
``
ladder.corrector(my.plants, to.correct="MN214c20_D06_QX.fsa", my.ladder)
``

##### Finally, we can now have a look at our data set. Use the overview function to view all your plants in one panel. In case you have a mixture of differentially labeled microsat primers, you can set the channel accrding to the color of the fluorophore (channel= 1 or 2 or 3 or 4). You can also set the size range you want to have a look at at (use xlim=c(160,190) ). I would suggest to first use 30-300. Then narrow donw your window. You can also select a subset of plants to look at using the my.plants[1:3] notation. Please, explore your data and note the potential size of the peaks.

``
overview(my.inds=my.plants, ladder=my.ladder, init.thresh=7000, xlim=c(160,190),channel=1)
``

##### Create your scoring panel using the overview2 function. This function will automatically find the peaks (alleles) and will assign them to a panel (allele sizes). This panel can be then saved and used later to score all your individuals automatically. You can also run this function only on a subset of individuals (e.g. my.plants[1:2). You will also get a plot, showing the peaks identified.
``
my.panel <- overview2(my.inds=my.plants, ladder=my.ladder, init.thresh=7000, xlim=c(170,190),channel=1) 
``

##### Automatic assignment/discovery of peaks is inaccurate and needs to be manually updated. Type " my.panel" to see the two peaks selected. To manually adjust the peaks detected, use the locator function. After hitting enter add the additional red dots to the plot to mark the undetected peaks. Hit escape once you are done wiht your corrections. We found that this approach will NOT update the my.panel vector. That is, it must be done manually. You have to enter and add the extra peak sizes to the vector manually (eg: my.panel$channel_1<-c(174.5792,178.8452,150,160).
``
my.panel <- locator(type="p", pch=20, col="red")$x
``

##### We now finally have our scoring panel so we can let the program score our fragment data. Use the score.markers function to do so. You can also do this only for a subset of individuals (use my.plants[1:3] instead of my.plants). The scoring parameters can be fine tuned. Just type scroe.marker and you will see all the parameters that can be modified to improve the accuracy of the scoring.
``
res <- score.markers(my.inds=my.plants, channel = 1, panel=my.panel$channel_1,ladder=my.ladder, electro=FALSE)
``

##### To export scoring results into a data.frame, use the final.results <- get.scores(a) function.
``
final.results <- get.scores(res)
``

##### Type final.results to see the results. You can use the write.table function to write it into a tab-delmited text file.


<br />
<br />



# 4. Read, correct and assemble Sanger sequencing files using the commercial software Sequencher

Sinple PCR products used to be sequenced using Sanger sequencing. Although Sanger sequencing is very accurate, sequencing used ti be done from both the 3 prime and 5 prime ends (forward and reverse). In this section, you will use the Sequencher software to create consensus sequences from the raw forward and reverse reads.

## Installation
First, download and install the trial version of the software. Both windows and mac versions are available (http://www.genecodes.com/free-download).


## Usage

Please, follow the Seqman section in the tutorial (Editing Sanger sequence chromatograms with SeqMan). I will give you a short intro to the usage of sequencher but using the instructions in the tutorial (written for seqman) should be sufficient to figure out how to use the software. If stuck, you can have a look at this short video (https://www.youtube.com/watch?v=BdJ686VC07Q).


<br />
<br />



# 5. Aligning contigs using Bioedit and aliview

Here, you will align contigs using Bioedit (only for windows users;https://bioedit.software.informer.com/) or aliview (for windows and mac; https://macdownload.informer.com/aliview/). First install these softwares on your computer.

After that follow the instructions given in the tutorial. Aliview has very similar options and Bioedit so it will be also easy to carry out the analyses for mac users. If stuck, you can also have a look at the folliwing video (https://www.youtube.com/watch?v=yN8p1gVMauM).






